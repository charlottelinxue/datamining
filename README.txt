1. Team Name: Team 5 Supernova
2. Team Members: Xue Lin, Xiaodong Zhou, Jiayi Zhu, Lei Wang
3. Project Description: Implementation of C4.5 Decision Tree
4. How to run the main script: 
(1) First, let the current directory of terminal be 
THE DIRECTORY OF the jar file ON YOUR COMPUTER
(2) Next, type command line: java -jar C45Team5.jar <TRAINING_DATA_PATH>
For example: java -jar C45Team5.jar /Users/Charlotte/Desktop/Data/trainProdSelection.arff
(3) Then, it will show guide lines of what to do next.
6. References material: Mitchell, T., Machine Learning.